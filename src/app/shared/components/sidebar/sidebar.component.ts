import { Component } from '@angular/core';
import { GifsService } from '../../../gifs/services/gifs.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css'
})
export class SidebarComponent {
  //Injeccion de independencias
  constructor( private gifsServices: GifsService){}

  get tags(){
    return this.gifsServices.tagsHistory
  }

  public gifsList = [...this.gifsServices.tagsHistory]
}

import { Component, ElementRef, ViewChild } from '@angular/core';
import { GifsService } from '../../services/gifs.service';

@Component({
  selector: 'gifs-search-box',
  templateUrl: './search-box.component.html',
})
export class SearchBoxComponent {

  @ViewChild('txtTagInput')
  public taginput!: ElementRef<HTMLInputElement>;

  constructor(private gifsServices: GifsService){}

  searchTag(){
    const newTag = this.taginput.nativeElement.value;

    this.gifsServices.searchTag(newTag);

    this.taginput.nativeElement.value = '';
    console.log({ newTag })
  }

}

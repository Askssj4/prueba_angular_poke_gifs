import { Component, Input } from '@angular/core';
import { Species, Sprites } from '../../interfaces/gifs.interfaces';

@Component({
  selector: 'gifs-card-list',
  templateUrl: './card-list.component.html',
})
export class CardListComponent {

  @Input()
  public gif: Species={
    name: '',
    url: ''
  };

  @Input()
  public gifSprites: Sprites={
    back_default: '',
    back_female: '',
    back_shiny: '',
    back_shiny_female: '',
    front_default: '',
    front_female: '',
    front_shiny: '',
    front_shiny_female: ''
  }

}

import { Component } from '@angular/core';
import { GifsService } from '../../services/gifs.service';
import { SearchResponse, Species } from '../../interfaces/gifs.interfaces';

@Component({
  selector: 'gifs-home-page',
  templateUrl: './home-page.component.html',
})
export class HomePageComponent {

  constructor(private gifsService: GifsService){

  }

  get gifs():Species{
    return this.gifsService.gifsList;
  }

  get gifSprites(){
    return  this.gifsService.gifSprites
  }

}

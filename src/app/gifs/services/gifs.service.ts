import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchResponse, Species, Sprites } from '../interfaces/gifs.interfaces';

const GIPHY_API_KEY = "SSB6xPxUcyXIyeKww3rEx7HyZvPmAHPQ"

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  public gifsList: Species={
    name: '',
    url: ''
  };

  public gifSprites: Sprites={
    back_default: '',
    back_female: '',
    back_shiny: '',
    back_shiny_female: '',
    front_default: '',
    front_female: '',
    front_shiny: '',
    front_shiny_female: ''
  }

  private apiKey: string = "SSB6xPxUcyXIyeKww3rEx7HyZvPmAHPQ";

  private _tagsHistory: string[] = [];

  constructor( private http: HttpClient) { }

  get tagsHistory(){
    return [...this._tagsHistory];
  }

  searchTag(tag: string):void{

    tag = tag.toLocaleLowerCase()

   if(tag!="")
   {  
      if(this._tagsHistory.includes(tag))
      {
        this._tagsHistory = this._tagsHistory.filter((oldTag)=>oldTag!==tag)
      }

      const header = new HttpHeaders({'Access-Control-Allow-Origin': '*'});


      //probar en mi pc
      // this.http.get(`http://api.giphy.com/v1/gifs/search?api_key=SSB6xPxUcyXIyeKww3rEx7HyZvPmAHPQ&q=dragonball&limit=10`, {headers:new HttpHeaders({'Access-Control-Allow-Origin': '*'})})
      // .subscribe(
      //   resp=>{
      //     console.log(resp);
          
      //   }
      // )

      this.http.get<SearchResponse>('https://pokeapi.co/api/v2/pokemon/'+tag)
      .subscribe(
        resp=>{
          console.log(resp.species);

          this.gifsList = resp.species;
          this.gifSprites = resp.sprites;

          console.log(this.gifsList);
          
          
        }
      )

      this._tagsHistory.unshift(tag)
      this._tagsHistory = this._tagsHistory.splice(0,10)
    

   }
    
  }

  
// getContent(subs: Array<string>): Observable<IContent> {
//   return (() => {
//     return this.http.get<IContent>( (() => {
//       let r = this.root;
//       subs.forEach((s, i, a) => {
//         if(i === a.length-1) {
//           r += s;
//         }
//         else {
//           if(s !== '/') {
//             r += s;
//           }
//         }
//       });
//       return r;
//     })(), {
//       headers: this.corsHeaders
//     });
   
//   })();
//   }

}
